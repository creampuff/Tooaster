# macaron
![status](https://img.shields.io/badge/build-error-lightgrey.svg) ![status](https://img.shields.io/badge/License-Apache-blue.svg) 


A current package-manager replacement for Creampuff OS.


#### The how and why.
I decided to develop new package manager.
We can't use any other package manager except for apt-get because Creampuff OS is developed based on Ubuntu.  
Certainly, apt-get is great and we should use it to manage package and its dependences.
However, considering smartphone use, is that good choice?  
Imagine how you install a new app from Store.
Probably, you could install app to tap INSTALL button only once. 
Did that procedure include typing system password, confirmation of system changes and alert of breaking dependences?
I guess answer is 'No'.
Installing app on smartphone is easy and fast.
Anyone, even if you're not good at computer, can operate it. 
Hence, Next generation package manager for mobile device use need to be developed.
Moreover, our second propose to make macaron is improve dependences management.
Of course, apt-get is manageable, easy, functional.
In my experience, however, when I install Cinnamon desktop environment recently, I broke dependences and Desktop went no to start because a

### Features 

- Software installation without superuser
- More secure and lightweight
- Efficient library link system
- Container update
- Safe removal

### Status 
This program is now under construction. So all function does not work currently. 

- [x] Develop macaron package format

- [x] Installation function

- [x] Develop package maker or builder



### Usage

#### Initial setup
>**WARNING** 
> Following guide does not work now!
> Development status will be displayed as an budge on top of this document.

First, check the installation and version running following command.
```
macaron -v
```
Latest version is 0.0.1. If the macaron is an older version, please update to latest version.
You can update package manager by running following command. This operation requires super user and dpkg which is Debian based operating systems default package manager.
```
macaron -y --check-pm-update
```

Basic setups already completed.

#### How to install software?
Vanilla macaron is not registered software repository that is pool of software data and meta informations. So you have to add repository before install new app. We can use following repository and register like following.

- [macaron Test Repository](https://dev-world.t2.lt/macaron/repo/)

```
macaron -y --add-repository URL
```
Then, install new app to run this command.
```
macaron -i firefox
```
Someone might think "Oh, we needn't update repository cache before install app?"
Yes, macaron don't need to update repository cache and wait during the system read this database.

